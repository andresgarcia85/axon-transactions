# README #

This sample application allows the creation of users and perform credit transactions between them. It is developed
using the [Axon Framework](https://github.com/AxonFramework/AxonFramework), therefore using patterns such
as [CQRS](https://martinfowler.com/bliki/CQRS.html) and
[Event Sourcing](https://martinfowler.com/eaaDev/EventSourcing.html).

### How do I get set up? ###

The application is developed with Spring Boot and uses `axon-spring-boot-starter`, which requires to have Axon Server
running:

* Download Axon Server from [here](https://download.axoniq.io/axonserver/AxonServer.zip)
* Unzip and run `java -jar axonserver.jar`
* Clone this repository `git clone https://podencogalactico@bitbucket.org/andresgarcia85/axon-transactions.git`
* Run `mvn spring-boot:run`

**Now you can start playing:**

* Access application API
  on [http://localhost:8081/api/swagger-ui/index.html](http://localhost:8081/api/swagger-ui/index.html). Some tips:
    * First create a couple of users.
    * Create a special transaction fromUser: "God" to any of the previous users in order to create money from scratch.
    * Then you can start making transactions of credit between them.
    * There is a special service called _AntiMoneyLaunderingService_ that checks and invalidates the 10% of
      transactions.
* Access h2 console on [http://localhost:8081/h2-console](http://localhost:8081/h2-console). When the application is
  loaded Axon will reapply all the events
  for each [Aggregate](https://docs.axoniq.io/reference-guide/axon-framework/axon-framework-commands/modeling/aggregate)
  in order to load its last status. That means that even if you remove all data from the USERS and
  TRANSACTIONS tables, the information will be automatically restored. To speed up this process Axon provides us with
  [Snapshots](https://docs.axoniq.io/reference-guide/axon-framework/tuning/event-snapshots).
* Access Axon Server Dashboard on [http://localhost:8024](http://localhost:8024). From here you will be able to query
  and visualize all the
  [Events](https://docs.axoniq.io/reference-guide/axon-framework/events).

