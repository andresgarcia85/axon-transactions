package org.example.axon.api;

import lombok.RequiredArgsConstructor;
import org.axonframework.commandhandling.gateway.CommandGateway;
import org.axonframework.messaging.responsetypes.ResponseTypes;
import org.axonframework.queryhandling.QueryGateway;
import org.example.axon.api.request.CreateTransactionRequest;
import org.example.axon.core.command.CreateTransactionCommand;
import org.example.axon.core.value.TransactionId;
import org.example.axon.query.transaction.TransactionView;
import org.example.axon.query.transaction.query.FindAllTransactionsQuery;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.concurrent.CompletableFuture;

@RestController
@RequestMapping("transactions")
@RequiredArgsConstructor
public class TransactionController {

    private final CommandGateway commandGateway;

    private final QueryGateway queryGateway;

    @GetMapping
    public CompletableFuture<List<TransactionView>> findAll() {
        return queryGateway.query(new FindAllTransactionsQuery(), ResponseTypes.multipleInstancesOf(TransactionView.class));
    }

    @PostMapping
    public CompletableFuture<String> createTransaction(@RequestBody CreateTransactionRequest request) {
        return commandGateway.send(new CreateTransactionCommand(
                new TransactionId(),
                request.getCredit(),
                request.getFromUser(),
                request.getToUser()));
    }


}
