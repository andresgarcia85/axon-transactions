package org.example.axon.api;

import lombok.RequiredArgsConstructor;
import org.axonframework.commandhandling.gateway.CommandGateway;
import org.axonframework.messaging.responsetypes.ResponseTypes;
import org.axonframework.queryhandling.QueryGateway;
import org.example.axon.api.request.CreateUserRequest;
import org.example.axon.core.command.CreateUserCommand;
import org.example.axon.core.value.UserId;
import org.example.axon.query.user.UserView;
import org.example.axon.query.user.query.FindAllUsersQuery;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.concurrent.CompletableFuture;

@RestController
@RequestMapping("users")
@RequiredArgsConstructor
public class UserController {

    private final CommandGateway commandGateway;

    private final QueryGateway queryGateway;

    @GetMapping
    public CompletableFuture<List<UserView>> findAll() {
        return queryGateway.query(new FindAllUsersQuery(), ResponseTypes.multipleInstancesOf(UserView.class));
    }

    @PostMapping
    public CompletableFuture<String> createUser(@RequestBody CreateUserRequest request) {
        return commandGateway.send(new CreateUserCommand(new UserId(), request.getName()));
    }

}
