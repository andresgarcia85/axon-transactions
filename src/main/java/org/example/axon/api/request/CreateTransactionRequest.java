package org.example.axon.api.request;

import lombok.Data;
import org.example.axon.core.value.UserId;

@Data
public class CreateTransactionRequest {

    UserId fromUser;

    UserId toUser;

    Integer credit;

}
