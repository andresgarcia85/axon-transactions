package org.example.axon.api.request;

import lombok.Data;

@Data
public class CreateUserRequest {

    String name;

}
