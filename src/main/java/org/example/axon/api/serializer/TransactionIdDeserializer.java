package org.example.axon.api.serializer;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import org.example.axon.core.value.TransactionId;
import org.springframework.boot.jackson.JsonComponent;

import java.io.IOException;

@JsonComponent
public class TransactionIdDeserializer extends JsonDeserializer<TransactionId> {
    @Override
    public TransactionId deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        return TransactionId.of(jsonParser.getValueAsString());
    }
}
