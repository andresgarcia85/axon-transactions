package org.example.axon.api.serializer;


import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import org.example.axon.core.value.TransactionId;
import org.springframework.boot.jackson.JsonComponent;

import java.io.IOException;

@JsonComponent
public class TransactionIdSerializer extends JsonSerializer<TransactionId> {
    @Override
    public void serialize(TransactionId transactionId, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        jsonGenerator.writeString(transactionId.toString());
    }
}
