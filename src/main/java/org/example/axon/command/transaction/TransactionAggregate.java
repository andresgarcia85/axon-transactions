package org.example.axon.command.transaction;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.axonframework.commandhandling.CommandHandler;
import org.axonframework.eventsourcing.EventSourcingHandler;
import org.axonframework.modelling.command.AggregateIdentifier;
import org.axonframework.spring.stereotype.Aggregate;
import org.example.axon.core.command.CancelTransactionCommand;
import org.example.axon.core.command.ConfirmTransactionCommand;
import org.example.axon.core.command.CreateTransactionCommand;
import org.example.axon.core.event.TransactionCanceledEvent;
import org.example.axon.core.event.TransactionConfirmedEvent;
import org.example.axon.core.event.TransactionCreatedEvent;
import org.example.axon.core.value.TransactionId;
import org.example.axon.core.value.TransactionStatus;
import org.example.axon.core.value.UserId;

import static org.axonframework.modelling.command.AggregateLifecycle.apply;

@Aggregate
@NoArgsConstructor // Necessary for Axon
public class TransactionAggregate {

    @Getter
    @AggregateIdentifier
    private TransactionId transactionId;

    @Getter
    private Integer credit;

    @Getter
    private UserId fromUser;

    @Getter
    private UserId toUser;

    @Getter
    private TransactionStatus status;

    @CommandHandler
    public TransactionAggregate(CreateTransactionCommand command) {
        apply(new TransactionCreatedEvent(
                command.getTransactionId(),
                command.getCredit(),
                command.getFromUser(),
                command.getToUser()));
    }

    @CommandHandler
    public void handle(ConfirmTransactionCommand command) {
        apply(new TransactionConfirmedEvent(
                command.getTransactionId()
        ));
    }

    @CommandHandler
    public void handle(CancelTransactionCommand command) {
        apply(new TransactionCanceledEvent(
                command.getTransactionId()
        ));
    }

    @EventSourcingHandler
    public void on(TransactionCreatedEvent event) {
        transactionId = event.getTransactionId();
        credit = event.getCredit();
        fromUser = event.getFromUser();
        toUser = event.getToUser();
    }

    @EventSourcingHandler
    public void on(TransactionConfirmedEvent event) {
        status = TransactionStatus.CONFIRMED;
    }

    @EventSourcingHandler
    public void on(TransactionCanceledEvent event) {
        status = TransactionStatus.FAILED;
    }
}
