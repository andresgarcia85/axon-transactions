package org.example.axon.command.transaction;

import org.axonframework.commandhandling.gateway.CommandGateway;
import org.axonframework.modelling.saga.EndSaga;
import org.axonframework.modelling.saga.SagaEventHandler;
import org.axonframework.modelling.saga.StartSaga;
import org.axonframework.spring.stereotype.Saga;
import org.example.axon.core.command.AddCreditCommand;
import org.example.axon.core.command.CancelTransactionCommand;
import org.example.axon.core.command.ConfirmTransactionCommand;
import org.example.axon.core.command.DeductCreditCommand;
import org.example.axon.core.event.*;
import org.example.axon.core.exception.InvalidTransactionException;
import org.example.axon.core.value.TransactionId;
import org.example.axon.core.value.UserId;
import org.example.axon.thirdparty.AntiMoneyLaunderingService;
import org.springframework.beans.factory.annotation.Autowired;

import static org.axonframework.modelling.saga.SagaLifecycle.associateWith;

@Saga
public class TransactionSaga {

    private static final String TRANSACTION_ID = "transactionId";

    private TransactionId transactionId;

    private UserId fromUser;

    private UserId toUser;

    @Autowired
    private transient CommandGateway commandGateway;

    @Autowired
    private transient AntiMoneyLaunderingService antiMoneyLaunderingService;

    @StartSaga
    @SagaEventHandler(associationProperty = TRANSACTION_ID)
    public void handle(TransactionCreatedEvent event) {

        associateWith(TRANSACTION_ID, event.getTransactionId() + "");

        transactionId = event.getTransactionId();
        fromUser = event.getFromUser();
        toUser = event.getToUser();

        if (UserId.of("God").equals(fromUser)) {
            commandGateway.send(new AddCreditCommand(
                    event.getToUser(),
                    event.getTransactionId(),
                    event.getCredit()));
        } else {
            try {
                antiMoneyLaunderingService.validateTransaction();
                commandGateway.send(new DeductCreditCommand(
                        event.getFromUser(),
                        event.getTransactionId(),
                        event.getCredit())
                ).exceptionally(throwable -> commandGateway.send(new CancelTransactionCommand(event.getTransactionId())));
            } catch (InvalidTransactionException e) {
                commandGateway.send(new CancelTransactionCommand(event.getTransactionId()));
            }
        }
    }

    @SagaEventHandler(associationProperty = TRANSACTION_ID)
    public void handle(CreditDeductedEvent event) {
        commandGateway.send(new AddCreditCommand(
                toUser,
                event.getTransactionId(),
                event.getCredit())
        ).exceptionally(throwable -> {
                    // Compensation
                    commandGateway.send(new AddCreditCommand(fromUser, event.getTransactionId(), event.getCredit()));
                    return commandGateway.send(new CancelTransactionCommand(transactionId));
                }
        );
    }

    @SagaEventHandler(associationProperty = TRANSACTION_ID)
    public void handle(CreditAddedEvent event) {
        commandGateway.send(new ConfirmTransactionCommand(transactionId));
    }

    @EndSaga
    @SagaEventHandler(associationProperty = TRANSACTION_ID)
    public void handle(TransactionCanceledEvent event) {
        // this event ends the Saga
    }

    @EndSaga
    @SagaEventHandler(associationProperty = TRANSACTION_ID)
    public void handle(TransactionConfirmedEvent event) {
        // this event ends the Saga
    }


}
