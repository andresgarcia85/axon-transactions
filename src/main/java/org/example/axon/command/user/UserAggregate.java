package org.example.axon.command.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.axonframework.commandhandling.CommandHandler;
import org.axonframework.eventsourcing.EventSourcingHandler;
import org.axonframework.modelling.command.AggregateIdentifier;
import org.axonframework.spring.stereotype.Aggregate;
import org.example.axon.core.command.AddCreditCommand;
import org.example.axon.core.command.CreateUserCommand;
import org.example.axon.core.command.DeductCreditCommand;
import org.example.axon.core.event.CreditAddedEvent;
import org.example.axon.core.event.CreditDeductedEvent;
import org.example.axon.core.event.UserCreatedEvent;
import org.example.axon.core.exception.NegativeCreditException;
import org.example.axon.core.exception.NotEnoughCreditException;
import org.example.axon.core.value.UserId;

import static org.axonframework.modelling.command.AggregateLifecycle.apply;

@Aggregate
@NoArgsConstructor // Necessary for Axon
public class UserAggregate {

    @Getter
    @AggregateIdentifier
    private UserId userId;

    @Getter
    private String name;

    @Getter
    private Integer credit;

    @CommandHandler
    public UserAggregate(CreateUserCommand command) {
        apply(new UserCreatedEvent(
                command.getUserId(),
                command.getName(),
                0)
        );
    }

    @CommandHandler
    public void handle(AddCreditCommand command) {

        if (command.getCredit() < 0) {
            throw new NegativeCreditException();
        }

        apply(new CreditAddedEvent(
                command.getUserId(),
                command.getTransactionId(),
                command.getCredit())
        );
    }

    @CommandHandler
    public void handle(DeductCreditCommand command) {

        if (command.getCredit() < 0) {
            throw new NegativeCreditException();
        }

        if ((credit - command.getCredit()) < 0) {
            throw new NotEnoughCreditException();
        }

        apply(new CreditDeductedEvent(
                command.getUserId(),
                command.getTransactionId(),
                command.getCredit())
        );
    }

    @EventSourcingHandler
    public void on(UserCreatedEvent event) {
        userId = event.getUserId();
        name = event.getName();
        credit = event.getCredit();
    }

    @EventSourcingHandler
    public void on(CreditAddedEvent event) {
        credit += event.getCredit();
    }

    @EventSourcingHandler
    public void on(CreditDeductedEvent event) {
        credit -= event.getCredit();
    }

}
