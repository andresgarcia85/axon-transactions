package org.example.axon.configuration;

import lombok.val;
import org.axonframework.commandhandling.CommandBus;
import org.axonframework.commandhandling.SimpleCommandBus;
import org.axonframework.config.EventProcessingConfigurer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AxonConfig {

    @Bean
    public CommandBus configureCommandBus() {
        val commandBus = SimpleCommandBus.builder().build();
        commandBus.registerDispatchInterceptor(new LoggingInterceptor<>());
        return commandBus;
    }

    @Autowired
    public void configureEventProcessing(EventProcessingConfigurer config) {
        val loggingInterceptor = new LoggingInterceptor<>();
        config.registerTrackingEventProcessor("users")
                .registerTrackingEventProcessor("transactions")
                .registerHandlerInterceptor("users", configuration -> loggingInterceptor)
                .registerHandlerInterceptor("transactions", configuration -> loggingInterceptor);
    }

}
