package org.example.axon.configuration;

import lombok.extern.log4j.Log4j2;
import lombok.val;
import org.axonframework.messaging.InterceptorChain;
import org.axonframework.messaging.Message;
import org.axonframework.messaging.MessageDispatchInterceptor;
import org.axonframework.messaging.MessageHandlerInterceptor;
import org.axonframework.messaging.unitofwork.UnitOfWork;

import java.util.List;
import java.util.function.BiFunction;

@Log4j2
public class LoggingInterceptor<T extends Message<?>> implements MessageDispatchInterceptor<T>, MessageHandlerInterceptor<T> {

    public BiFunction<Integer, T, T> handle(List<? extends T> messages) {
        return (i, message) -> {
            log.info("Dispatched message: [{}]", message.getPayload());
            return message;
        };
    }

    public Object handle(UnitOfWork<? extends T> unitOfWork, InterceptorChain interceptorChain) throws Exception {
        val message = unitOfWork.getMessage();
        log.info("Incoming message: [{}]", message.getPayload());

        try {
            val returnValue = interceptorChain.proceed();
            log.info("[{}] executed successfully", message.getPayloadType().getSimpleName());
            return returnValue;
        } catch (Exception e) {
            log.warn(String.format("[%s] execution failed:", message.getPayloadType().getSimpleName()), e);
            throw e;
        }
    }
}
