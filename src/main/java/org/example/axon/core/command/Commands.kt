package org.example.axon.core.command

import org.axonframework.modelling.command.TargetAggregateIdentifier
import org.example.axon.core.value.TransactionId
import org.example.axon.core.value.UserId


data class AddCreditCommand(@field:TargetAggregateIdentifier val userId: UserId, val transactionId: TransactionId, val credit: Int)
data class CancelTransactionCommand(@field:TargetAggregateIdentifier val transactionId: TransactionId)
data class ConfirmTransactionCommand(@field:TargetAggregateIdentifier val transactionId: TransactionId)
data class CreateTransactionCommand(@field:TargetAggregateIdentifier val transactionId: TransactionId, val credit: Int, val fromUser: UserId, val toUser: UserId)
data class CreateUserCommand(@field:TargetAggregateIdentifier val userId: UserId, val name: String)
data class DeductCreditCommand(@field:TargetAggregateIdentifier val userId: UserId, val transactionId: TransactionId, val credit: Int)