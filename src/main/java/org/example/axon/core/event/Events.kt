package org.example.axon.core.event

import org.example.axon.core.value.TransactionId
import org.example.axon.core.value.UserId

data class CreditAddedEvent(val userId: UserId, val transactionId: TransactionId, val credit: Int)
data class CreditDeductedEvent(val userId: UserId, val transactionId: TransactionId, val credit: Int)
data class TransactionCanceledEvent(val transactionId: TransactionId)
data class TransactionConfirmedEvent(val transactionId: TransactionId)
data class TransactionCreatedEvent(val transactionId: TransactionId, val credit: Int, val fromUser: UserId, val toUser: UserId)
data class UserCreatedEvent(val userId: UserId, val name: String, val credit: Int)