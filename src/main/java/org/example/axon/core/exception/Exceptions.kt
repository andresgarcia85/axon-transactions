package org.example.axon.core.exception

class InvalidTransactionException : Exception()
class NegativeCreditException : RuntimeException()
class NotEnoughCreditException : RuntimeException()
