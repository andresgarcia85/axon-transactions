package org.example.axon.core.value;

public enum TransactionStatus {
    CREATED,
    CONFIRMED,
    FAILED;
}
