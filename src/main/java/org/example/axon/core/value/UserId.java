package org.example.axon.core.value;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import org.axonframework.common.IdentifierFactory;

import java.io.Serializable;

import static lombok.AccessLevel.NONE;


@Value
@RequiredArgsConstructor(staticName = "of")
public class UserId implements Serializable {

    @Getter(NONE)
    String id;

    public UserId() {
        id = IdentifierFactory.getInstance().generateIdentifier();
    }

    @Override
    public String toString() {
        return id;
    }

}
