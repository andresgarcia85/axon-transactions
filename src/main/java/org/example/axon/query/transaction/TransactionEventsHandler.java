package org.example.axon.query.transaction;

import lombok.RequiredArgsConstructor;
import lombok.val;
import org.axonframework.config.ProcessingGroup;
import org.axonframework.eventhandling.EventHandler;
import org.axonframework.queryhandling.QueryHandler;
import org.example.axon.core.event.TransactionCanceledEvent;
import org.example.axon.core.event.TransactionConfirmedEvent;
import org.example.axon.core.event.TransactionCreatedEvent;
import org.example.axon.core.value.TransactionStatus;
import org.example.axon.query.exception.ResourceNotFoundException;
import org.example.axon.query.transaction.query.FindAllTransactionsQuery;
import org.springframework.stereotype.Service;

@Service
@ProcessingGroup("transactions")
@RequiredArgsConstructor
public class TransactionEventsHandler {

    private final TransactionViewRepository transactionViewRepository;

    @EventHandler
    public void on(TransactionCreatedEvent event) {
        val transactionView = new TransactionView();
        transactionView.setTransactionId(event.getTransactionId());
        transactionView.setCredit(event.getCredit());
        transactionView.setFromUser(event.getFromUser());
        transactionView.setToUser(event.getToUser());
        transactionView.setStatus(TransactionStatus.CREATED);
        transactionViewRepository.save(transactionView);
    }

    @EventHandler
    public void on(TransactionConfirmedEvent event) {
        val transaction = transactionViewRepository.findById(event.getTransactionId())
                .orElseThrow(ResourceNotFoundException::new);
        transaction.setStatus(TransactionStatus.CONFIRMED);
        transactionViewRepository.save(transaction);
    }

    @EventHandler
    public void on(TransactionCanceledEvent event) {
        val transaction = transactionViewRepository.findById(event.getTransactionId())
                .orElseThrow(ResourceNotFoundException::new);
        transaction.setStatus(TransactionStatus.FAILED);
        transactionViewRepository.save(transaction);
    }

    @QueryHandler
    public Iterable<TransactionView> handle(FindAllTransactionsQuery query) {
        return transactionViewRepository.findAll();
    }

}
