package org.example.axon.query.transaction;


import lombok.Data;
import org.example.axon.core.value.TransactionId;
import org.example.axon.core.value.TransactionStatus;
import org.example.axon.core.value.UserId;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.time.LocalDateTime;

@Data
@Entity
public class TransactionView {

    @Id
    private TransactionId transactionId;

    private Integer credit;

    private UserId fromUser;

    private UserId toUser;

    private TransactionStatus status;

    @CreationTimestamp
    private LocalDateTime createdOn;

    @UpdateTimestamp
    private LocalDateTime modifiedOn;

}
