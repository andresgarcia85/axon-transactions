package org.example.axon.query.transaction;

import org.example.axon.core.value.TransactionId;
import org.springframework.data.repository.CrudRepository;

public interface TransactionViewRepository extends CrudRepository<TransactionView, TransactionId> {
}
