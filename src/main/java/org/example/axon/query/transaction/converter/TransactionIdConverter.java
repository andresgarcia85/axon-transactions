package org.example.axon.query.transaction.converter;

import org.example.axon.core.value.TransactionId;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class TransactionIdConverter implements AttributeConverter<TransactionId, String> {
    @Override
    public String convertToDatabaseColumn(TransactionId transactionId) {
        return transactionId.toString();
    }

    @Override
    public TransactionId convertToEntityAttribute(String transactionId) {
        return TransactionId.of(transactionId);
    }
}
