package org.example.axon.query.user;

import lombok.RequiredArgsConstructor;
import lombok.val;
import org.axonframework.config.ProcessingGroup;
import org.axonframework.eventhandling.EventHandler;
import org.axonframework.queryhandling.QueryHandler;
import org.example.axon.core.event.CreditAddedEvent;
import org.example.axon.core.event.CreditDeductedEvent;
import org.example.axon.core.event.UserCreatedEvent;
import org.example.axon.query.exception.ResourceNotFoundException;
import org.example.axon.query.user.query.FindAllUsersQuery;
import org.springframework.stereotype.Service;

@Service
@ProcessingGroup("users")
@RequiredArgsConstructor
public class UserEventsHandler {

    private final UserViewRepository userViewRepository;

    @EventHandler
    public void on(UserCreatedEvent event) {
        val userView = new UserView();
        userView.setUserId(event.getUserId());
        userView.setName(event.getName());
        userView.setCredit(event.getCredit());
        userViewRepository.save(userView);
    }

    @EventHandler
    public void on(CreditAddedEvent event) {
        val userView = userViewRepository.findById(event.getUserId())
                .orElseThrow(ResourceNotFoundException::new);
        userView.addCredit(event.getCredit());
        userViewRepository.save(userView);
    }

    @EventHandler
    public void on(CreditDeductedEvent event) {
        val userView = userViewRepository.findById(event.getUserId())
                .orElseThrow(ResourceNotFoundException::new);
        userView.deductCredit(event.getCredit());
        userViewRepository.save(userView);
    }

    @QueryHandler
    public Iterable<UserView> handle(FindAllUsersQuery query) {
        return userViewRepository.findAll();
    }

}
