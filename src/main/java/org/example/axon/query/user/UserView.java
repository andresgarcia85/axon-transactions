package org.example.axon.query.user;


import lombok.Data;
import org.example.axon.core.value.UserId;

import javax.persistence.Entity;
import javax.persistence.Id;

@Data
@Entity
public class UserView {

    @Id
    private UserId userId;

    private String name;

    private Integer credit;

    public void addCredit(Integer credit) {
        this.credit += credit;
    }

    public void deductCredit(Integer credit) {
        this.credit -= credit;
    }

}
