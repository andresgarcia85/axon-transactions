package org.example.axon.query.user;

import org.example.axon.core.value.UserId;
import org.springframework.data.repository.CrudRepository;

public interface UserViewRepository extends CrudRepository<UserView, UserId> {
}
