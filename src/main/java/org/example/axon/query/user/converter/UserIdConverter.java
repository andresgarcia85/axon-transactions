package org.example.axon.query.user.converter;

import org.example.axon.core.value.UserId;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class UserIdConverter implements AttributeConverter<UserId, String> {
    @Override
    public String convertToDatabaseColumn(UserId userId) {
        return userId.toString();
    }

    @Override
    public UserId convertToEntityAttribute(String userId) {
        return UserId.of(userId);
    }
}
