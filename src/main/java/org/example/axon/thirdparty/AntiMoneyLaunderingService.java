package org.example.axon.thirdparty;

import lombok.RequiredArgsConstructor;
import lombok.val;
import org.example.axon.core.exception.InvalidTransactionException;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class AntiMoneyLaunderingService {

    public void validateTransaction() throws InvalidTransactionException {
        val random = Math.random();
        if (random < 0.1) {
            // 10% of the times it returns an exception
            throw new InvalidTransactionException();
        }
    }

}
