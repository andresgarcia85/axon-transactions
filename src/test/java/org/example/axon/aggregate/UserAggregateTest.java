package org.example.axon.aggregate;

import org.axonframework.test.aggregate.AggregateTestFixture;
import org.axonframework.test.aggregate.FixtureConfiguration;
import org.example.axon.command.user.UserAggregate;
import org.example.axon.core.command.AddCreditCommand;
import org.example.axon.core.command.CreateUserCommand;
import org.example.axon.core.command.DeductCreditCommand;
import org.example.axon.core.event.CreditAddedEvent;
import org.example.axon.core.event.CreditDeductedEvent;
import org.example.axon.core.event.UserCreatedEvent;
import org.example.axon.core.exception.NegativeCreditException;
import org.example.axon.core.exception.NotEnoughCreditException;
import org.example.axon.core.value.TransactionId;
import org.example.axon.core.value.UserId;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class UserAggregateTest {

    private static final UserId USER_ID = new UserId();
    private static final TransactionId TRANSACTION_ID = new TransactionId();
    private static final String RALPH = "Ralph";
    private static final Integer INITIAL_CREDIT = 1;
    private static final Integer CREDIT_TO_DEDUCT = 2;
    private static final Integer CREDIT_TO_ADD = 2;
    private static final Integer NEGATIVE_CREDIT = -2;

    private FixtureConfiguration<UserAggregate> fixture;

    @BeforeEach
    void setUp() {
        fixture = new AggregateTestFixture<>(UserAggregate.class);
    }

    @Nested
    class CreateUserCommandTest {

        @Test
        void handle() {
            fixture.givenNoPriorActivity()
                    .when(new CreateUserCommand(USER_ID, RALPH))
                    .expectEvents(new UserCreatedEvent(USER_ID, RALPH, 0))
                    .expectState(userAggregate -> {
                        assertThat(userAggregate.getUserId()).isEqualTo(USER_ID);
                        assertThat(userAggregate.getName()).isEqualTo(RALPH);
                        assertThat(userAggregate.getCredit()).isZero();
                    });
        }

    }

    @Nested
    class AddCreditCommandTest {

        @Test
        void handle() {
            fixture.given(new UserCreatedEvent(USER_ID, RALPH, 0))
                    .when(new AddCreditCommand(USER_ID, TRANSACTION_ID, CREDIT_TO_ADD))
                    .expectEvents(new CreditAddedEvent(USER_ID, TRANSACTION_ID, CREDIT_TO_ADD))
                    .expectState(userAggregate ->
                            assertThat(userAggregate.getCredit()).isEqualTo(CREDIT_TO_ADD)
                    );
        }

        @Test
        void handleNegativeCredit() {
            fixture.given(new UserCreatedEvent(USER_ID, RALPH, 0))
                    .when(new AddCreditCommand(USER_ID, TRANSACTION_ID, NEGATIVE_CREDIT))
                    .expectNoEvents()
                    .expectException(NegativeCreditException.class);
        }

    }

    @Nested
    class DeductCreditCommandTest {

        @Test
        void handle() {
            fixture.given(new UserCreatedEvent(USER_ID, RALPH, 0),
                            new CreditAddedEvent(USER_ID, TRANSACTION_ID, INITIAL_CREDIT + 2))
                    .when(new DeductCreditCommand(USER_ID, TRANSACTION_ID, CREDIT_TO_DEDUCT))
                    .expectEvents(new CreditDeductedEvent(USER_ID, TRANSACTION_ID, CREDIT_TO_DEDUCT))
                    .expectState(userAggregate ->
                            assertThat(userAggregate.getCredit()).isEqualTo(INITIAL_CREDIT + 2 - CREDIT_TO_DEDUCT)
                    );
        }

        @Test
        void handleNotEnoughCredit() {
            fixture.given(new UserCreatedEvent(USER_ID, RALPH, 0),
                            new CreditAddedEvent(USER_ID, TRANSACTION_ID, INITIAL_CREDIT))
                    .when(new DeductCreditCommand(USER_ID, TRANSACTION_ID, CREDIT_TO_DEDUCT))
                    .expectNoEvents()
                    .expectException(NotEnoughCreditException.class);
        }

        @Test
        void handleNegativeCredit() {
            fixture.given(new UserCreatedEvent(USER_ID, RALPH, 0))
                    .when(new DeductCreditCommand(USER_ID, TRANSACTION_ID, NEGATIVE_CREDIT))
                    .expectNoEvents()
                    .expectException(NegativeCreditException.class);
        }

    }

}