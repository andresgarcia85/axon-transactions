package org.example.axon.command.transaction;

import org.axonframework.test.aggregate.AggregateTestFixture;
import org.axonframework.test.aggregate.FixtureConfiguration;
import org.example.axon.core.command.CancelTransactionCommand;
import org.example.axon.core.command.ConfirmTransactionCommand;
import org.example.axon.core.command.CreateTransactionCommand;
import org.example.axon.core.event.TransactionCreatedEvent;
import org.example.axon.core.value.TransactionId;
import org.example.axon.core.value.UserId;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.example.axon.core.value.TransactionStatus.CONFIRMED;
import static org.example.axon.core.value.TransactionStatus.FAILED;

class TransactionAggregateTest {

    private static final UserId FROM_USER = new UserId();
    private static final UserId TO_USER = new UserId();
    private static final TransactionId TRANSACTION_ID = new TransactionId();
    private static final Integer CREDIT = 5;

    private FixtureConfiguration<TransactionAggregate> fixture;

    @BeforeEach
    void setUp() {
        fixture = new AggregateTestFixture<>(TransactionAggregate.class);
    }

    @Nested
    class CreateTransactionCommandTest {

        @Test
        void handle() {
            fixture.givenNoPriorActivity()
                    .when(new CreateTransactionCommand(TRANSACTION_ID, CREDIT, FROM_USER, TO_USER))
                    .expectEvents(new TransactionCreatedEvent(TRANSACTION_ID, CREDIT, FROM_USER, TO_USER))
                    .expectState(transactionAggregate -> {
                        assertThat(transactionAggregate.getTransactionId()).isEqualTo(TRANSACTION_ID);
                        assertThat(transactionAggregate.getCredit()).isEqualTo(CREDIT);
                        assertThat(transactionAggregate.getFromUser()).isEqualTo(FROM_USER);
                        assertThat(transactionAggregate.getToUser()).isEqualTo(TO_USER);
                    });
        }

    }

    @Nested
    class ConfirmTransactionCommandTest {

        @Test
        void handle() {
            fixture.given(new TransactionCreatedEvent(TRANSACTION_ID, CREDIT, FROM_USER, TO_USER))
                    .when(new ConfirmTransactionCommand(TRANSACTION_ID))
                    .expectState(transactionAggregate -> assertThat(transactionAggregate.getStatus()).isEqualTo(CONFIRMED));
        }

    }

    @Nested
    class CancelTransactionCommandTest {

        @Test
        void handle() {
            fixture.given(new TransactionCreatedEvent(TRANSACTION_ID, CREDIT, FROM_USER, TO_USER))
                    .when(new CancelTransactionCommand(TRANSACTION_ID))
                    .expectState(transactionAggregate -> assertThat(transactionAggregate.getStatus()).isEqualTo(FAILED));
        }

    }

}